import Vue from "vue";
import Router from "vue-router";
import BaseMain from "@/views/layout/BaseMain";

Vue.use(Router);
const routes = [
  {
    path: "/dashboard",
    component: () => import("@/views/pages/dashboard/Index"),
    meta: {
      requireAuth: true
    },
  }
];
export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: routes
});
