import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import VueI18n from "vue-i18n";
import VSwitch from "v-switch-case";
import VueHead from "vue-head";
import VueCookie from "vue-cookie";
import EventBus from "./utilities/event_bus";
import { DEFAULT_LOCALIZATION } from "./config";

Vue.config.productionTip = false;

Vue.use(VueI18n);
Vue.use(VSwitch);
Vue.use(VueHead);
Vue.use(VueCookie);
Vue.use(EventBus);

export function localeReady(callback) {
  let defaultLocale = DEFAULT_LOCALIZATION.locale;

  document.querySelector("html").setAttribute("lang", defaultLocale);

  import(`./locales/${defaultLocale}`).then(m => {
    callback(
      new VueI18n({
        locale: defaultLocale,
        fallbackLocale: defaultLocale,
        silentFallbackWarn: true,
        messages: (() => {
          const messages = {};
          messages[defaultLocale] = m.default;
          return messages;
        })()
      })
    );
  });
}

localeReady(i18n => {
  new Vue({
    i18n,
    router,
    store,
    render: h => h(App)
  }).$mount("#app");
});
