import { UserService } from "@/services/user";

export default {
  namespaced: true,
  state() {
    return {
      users: [],
      user: {}
    };
  },
  getters: {
    users: state => state.users,
    user: state => state.user
  },
  mutations: {
    setUser(state, { user }) {
      state.user = user;
    }
  },
  actions: {
    search({ state }, { params, doneCallback, errorCallback, alwaysCallback }) {
      UserService().index(
        params,
        data => {
          state.users = data.users;
          doneCallback(data.pagination);
        },
        errorCallback,
        alwaysCallback
      );
    },

    create({ state }, { params, doneCallback, errorCallback, alwaysCallback }) {
      UserService().store(
        params,
        data => {
          state.user = data.user;
          doneCallback();
        },
        errorCallback,
        alwaysCallback
      );
    },

    getById({ state }, { id, doneCallback, errorCallback, alwaysCallback }) {
      if (state.user.id && state.user.id === id) {
        doneCallback();
        return;
      }
      UserService().show(
        id,
        data => {
          state.user = data.user;
          doneCallback();
        },
        errorCallback,
        alwaysCallback
      );
    },

    edit(
      { state },
      { id, params, doneCallback, errorCallback, alwaysCallback }
    ) {
      UserService().update(
        id,
        params,
        data => {
          state.user = data.user;
          doneCallback();
        },
        errorCallback,
        alwaysCallback
      );
    },

    editRole(store, { id, role, doneCallback, errorCallback, alwaysCallback }) {
      UserService().updateRole(
        id,
        role,
        doneCallback,
        errorCallback,
        alwaysCallback
      );
    },

    destruct({ state }) {
      state.users = [];
      state.user = {};
    }
  }
};
