import axios from "axios";
import { APP_DEFAULT_SERVICE, APP_NAME, APP_URL } from "@/config";

export const HTTP = axios.create({
  baseURL: APP_DEFAULT_SERVICE.base_url,
  headers: (() => {
    const headers = {};
    headers[APP_DEFAULT_SERVICE.headers.application] = JSON.stringify({
      name: APP_NAME,
      url: APP_URL
    });
    if (APP_DEFAULT_SERVICE.basic_auth) {
      headers[APP_DEFAULT_SERVICE.headers.basic_authorization] =
        "Basic " + btoa(APP_DEFAULT_SERVICE.basic_auth);
    }
    return headers;
  })()
})

export default class BaseService {
  constructor(basePath = null) {
    this.basePath = basePath;
  }

  path(relativePath = null) {
    let split = this.basePath && relativePath ? "/" : "";
    return this.basePath ? this.basePath + split + relativePath : relativePath;
  }

  done(response, doneCallback = null) {
    if (doneCallback) doneCallback(response);
  }

  error(error, errorCallback = null) {
    if (errorCallback) errorCallback(error);
  }

  always(alwaysCallback = null) {
    if (alwaysCallback) alwaysCallback();
  }

  get(
    path,
    params = {},
    doneCallback = null,
    errorCallback = null,
    alwaysCallback = null,
    cancelToken = null
  ) {
    return HTTP
      .get(this.path(path), {
        params: this.params(params),
        cancelToken: cancelToken
      })
      .then(response => {
        this.done(response, doneCallback);
      })
      .catch(error => {
        this.error(error, errorCallback);
      })
      .then(() => {
        this.always(alwaysCallback);
      });
  }

  post(
    path,
    params = {},
    doneCallback = null,
    errorCallback = null,
    alwaysCallback = null,
    cancelToken = null
  ) {
    return HTTP
      .post(this.path(path), this.params(params), {
        cancelToken: cancelToken
      })
      .then(response => {
        this.done(response, doneCallback);
      })
      .catch(error => {
        this.error(error, errorCallback);
      })
      .then(() => {
        this.always(alwaysCallback);
      });
  }

  put(
    path,
    params = {},
    doneCallback = null,
    errorCallback = null,
    alwaysCallback = null,
    cancelToken = null
  ) {
    return HTTP
      .put(this.path(path), this.params(params), {
        cancelToken: cancelToken
      })
      .then(response => {
        this.done(response, doneCallback);
      })
      .catch(error => {
        this.error(error, errorCallback);
      })
      .then(() => {
        this.always(alwaysCallback);
      });
  }

  delete(
    path,
    params = {},
    doneCallback = null,
    errorCallback = null,
    alwaysCallback = null,
    cancelToken = null
  ) {
    return HTTP
      .delete(this.path(path), {
        params: this.params(params),
        cancelToken: cancelToken
      })
      .then(response => {
        this.done(response, doneCallback);
      })
      .catch(error => {
        this.error(error, errorCallback);
      })
      .then(() => {
        this.always(alwaysCallback);
      });
  }
}
