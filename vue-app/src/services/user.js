import BaseService from "./base";

export class UserService extends BaseService {
  constructor() {
    super("user");
  }

  index(
    params = {},
    doneCallback = null,
    errorCallback = null,
    alwaysCallback = null
  ) {
    this.get("", params, doneCallback, errorCallback, alwaysCallback);
  }

  store(
    params = {},
    doneCallback = null,
    errorCallback = null,
    alwaysCallback = null
  ) {
    this.post("", params, doneCallback, errorCallback, alwaysCallback);
  }

  show(id, doneCallback = null, errorCallback = null, alwaysCallback = null) {
    this.get(id, {}, doneCallback, errorCallback, alwaysCallback);
  }

  update(
    id,
    params = {},
    doneCallback = null,
    errorCallback = null,
    alwaysCallback = null
  ) {
    this.put(id, params, doneCallback, errorCallback, alwaysCallback);
  }

  destroy(
    ids,
    params = {},
    doneCallback = null,
    errorCallback = null,
    alwaysCallback = null
  ) {
    params.ids = ids;
    this.delete("", params, doneCallback, errorCallback, alwaysCallback);
  }
}
export const userAdminService = () => new UserService();
