export const DEFAULT_LOCALIZATION = {
  locale: "en",
  short_date_js_format: "YYYY-MM-DD"
};
export const APP_DEFAULT_SERVICE = {
  base_url: process.env.VUE_APP_SERVICE_URL,
  basic_auth: process.env.VUE_APP_SERVICE_HEADER_BASIC_AUTHORIZATION,
  headers: {
    application: process.env.VUE_APP_SERVICE_HEADER_APPLICATION_NAME,
    localization: process.env.VUE_APP_SERVICE_HEADER_LOCALIZATION_NAME,
    token_authorization:
      process.env.VUE_APP_SERVICE_HEADER_TOKEN_AUTHORIZATION_NAME,
    basic_authorization:
      process.env.VUE_APP_SERVICE_HEADER_BASIC_AUTHORIZATION_NAME
  }
};
export const APP_NAME = process.env.VUE_APP_NAME;
export const APP_URL =
  window.location.origin +
  (process.env.VUE_APP_ENV === "production" ? "/" : "");
export const APP_COOKIE = {
  names: {
    default: "vue_app",
    localization: "vue_app_localization"
  },
  secret: process.env.VUE_APP_COOKIE_SECRET,
  domain: process.env.VUE_APP_COOKIE_DOMAIN
};
