export default {
  hello: "Hello",
  user: "User | Users",
  form_name: "Form's Name",
  verify_email_done:
    'Your e-mail address <span class="text-info">{email}</span> is verified!'
};
