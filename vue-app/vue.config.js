const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
    outputDir: './public/',
    publicPath: process.env.VUE_APP_ENV === 'production' ? '/admin/' : '/',
    productionSourceMap: false,
    configureWebpack: {
        optimization: {
            minimizer: [
                new TerserPlugin({
                    terserOptions: {
                        output: {
                            comments: false,
                        },
                    },
                }),
            ],
        },
    }
}
