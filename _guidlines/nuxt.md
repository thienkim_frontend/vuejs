## FEATURES
combine libraries
organize my code: has standard folder structure; automatically generates Vue Router configuration
optimize for SEO/ correct index by Search enginer
optimize for speed

## INSTALLATION  
Prerequsite: npm > 5.2.0 (npx is shipped by default since NPM 5.2.0)
```
yarn create nuxt-app <project-name>
npx create-nutx-app <project-name>

PORT=3001 npm run dev
```

Install plugin with available project  
```
npm install 
npm install @nuxtjs/axios
```  

To generate our web application into static files  
```
npm run generate 
```



To build & start for production:  
```
npm run build
npm start
```
## FOLDER STRUCTURE
https://nuxtjs.org/guide/directory-structure

```
assets/
----- img/            // Images and icons for your app
----- css/            // All styles and style related files (SCSS files)
--------------- _variable.scss
--------------- _mixin.scss
--------------- style.scss
----- fonts/       
components/           // You can't use either asyncData or fetch in these components.
layouts/              // Make sure to add the <nuxt/> component when creating a layout to actually include the page component.
----- default.vue     // It will be used for all pages that don't have a layout specified. 
----- blog.vue        // Every file (top-level) in the layouts directory will create a custom layout accessible with the layout property in the page components
----- error.vue       // The custom layout for the error page
middleware            // define custom functions that can be run before rendering even a page or a group of pages
pages/                // Nuxt will automatically create the application router
plugins               // These plugins that run before instantiating the route vue instance. This is the place to register components globally or to inject functions or constants
static/               // The files will automatically be mapped to the server's root
store/                // The store is disabled by default & You can safely delete this directory
----- index.js        // Creating an index.js file in this directory enables the store.
nuxt.config.js        // define all default <meta> tags for your application using the same head property
.eslintrc.js
.prettierrc
README.md
```  
These folder are in root  
- pages: contains the top level views (in .vue files). Used to generate routes  
  Nuxt.js will listen for file changes inside the pages directory, so there is no need to restart the application when adding new pages.  
  File-based Routing. The files can be renamed without extra configuration  
  Nutx with SSR gives proper status codes (404 not found)  
- plugins: javascript plugins to load before starting the Vue app
- static: robots.txt or favicon
- store: creating an index.js file in this directory enables the store  
  every .js file inside the store directory is transformed as a namespaced module (index being the root module)
- middleware: custom functions to run before rendering a layout or page
- layouts  
  Add global styles into /layout/default.vue  
- assets: sass, images, or fonts
  By default, Nutx is using vue-loader, file-loader, and url-loader for effective asset serving  
  On build if image file is >= 1kb: uses version hashes for caching  
  &lt;img src="/_nuxt/img/82f7965.png"&gt;  
  On build if image file is < 1kb: inline's the image to reduce http requests  
- components: contains your reuseable Vue components  
  Nutx adds component functionality based on which directory your components is in
- nutx.config.js Used to modify the default nuxt configuration  
  Fix settings in VScode - conflict between vetur and eslint

## SPA vs UNIVERSAL MODE
- SPA aren't fast on initial load
  1. download the index.html
  1. download all of the Vue app js
  1. initialize the Vue app
  1. initialize Vue router and route to proper components
  1. do any additional API calls to fetch data to render
  1. render templates to the page  

--spa or -s: Runs command in SPA mode and disables server side rendering  
Pro tip: to pass arguments to npm commands, you need an extra -- script name.  
```  
npm run dev -- --spa
```

- Universal Mode: increases speed of initial page load  
  User: types your-web.com/create 
  1. Browser: request Url => render Vue components to .html (vue-meta updates title & description)
  1. Returns create.html => Show new page (It's fast because Vue isn't loaded yet)
  1. Request Js files => Sever: Return Js => Start up Vue

Nutx code-splitting => Faster loading: not all templates loaded upfront  
After initial load + hydration it behaves like a normal SPA  

Smart fetching (Nuts 2.4): Nutx.js will automatically prefetch the code-splitted pages linked with `<nuxt-link>` when visible in the viewport by default  
Prefetching increases speed of pages that are code-split

## Help
  ESLINT
  You may use special comments to disable some warnings.                                                                                                       
  Use // eslint-disable-next-line to ignore the next line.                                                                                                     
  Use /* eslint-disable */ to ignore all warnings in a file.

  **ERROR: dependency issue: not found .nuxt/client.js**
  SOLUTION: You must install peer dependencies yourself.

  **ERROR: 7 errors, 0 warnings potentially fixable with the `--fix` option.**
  SOLUTION: adding fix option in nuxt.config.js

## VUE META
https://vue-meta.nuxtjs.org/api/#title
Nuxt.js uses vue-meta to update the headers and html attributes of your application.
Your component data are available with this in the head method, you can use set custom meta tags with the page data.

```vue
<template>
  <h1>{{ title }}</h1>
</template>

<script>
export default {
  data () {
    return {
      title: 'Hello World!'
    }
  },
  head () {
    return {
      title: this.title,
      meta: [
        // hid is used as unique identifier. Do not use `vmid` for it as it will not work
        { hid: 'description', name: 'description', content: 'My custom description' },
        { name: 'viewport', content: 'width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no' }
      ],
      link: [
        { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto' }
      ]
    }
  }
}
</script>
```

## AXIOS
- Make XMLHttpRequests from the browser
- Make http requests from node.js
- Supports the Promise API
- Intercept request and response
- Transform request and response data
- Cancel requests
- Automatic transforms for JSON data
- Client side support for protecting against XSRF

**you’re able to register callback functions for onUploadProgress and onDownloadProgress to display the percentage complete in your app’s UI**

The response object:
- data: the payload returned from the server. By default, Axios expects JSON and will parse this back into a javascript object for you.
- status: the HTTP code returned from the server.
- statusText: the HTTP status message returned by the server.
- headers: all the headers sent back by the server.
- config: the original request configuration.
- request: the actual XMLHttpRequest object (when running in a browser).

The error object
- message: the error message text.
- response: the response object (if received) as described in the previous section.
- request: the actual XMLHttpRequest object (when running in a browser).
- config: the original request configuration.

## VUEX 
Nuxt by default configures Vuex to use strict mode in development  
Nuxt provides a fetch method in pages. This is very useful for hooking into actions prior to page rendering  

[vuex] do not mutate vuex store state outside mutation handlers.

**Mutating deeply nested values is also error prone and likely to cause Vuex to reject the mutation**
Ex: $store.state.cars.car.tire.brand  
Solution: Create new stores when deep nesting mutations are used => to keep your state object as flat as possible  
create a state contains the attributes for a single car entity   

**ERROR: v-model=”$state.cars.car.color” which won’t work because this is effectively the read-only version of the value**
```javascript
export const state = () => ({
  list: [],
  car: {}
})
```

```javascript
// Getters are read-only and typically useful for formatting data
export const getters = {
  totalCars: state => {
    return state.cars.length
  },
  blueCars: state => {
    return state.cars.filter(car => car.color === "blue")
  }
}

export const mutations = {
  set(state, cars) {
    state.list = cars
  },
  add(state, value) {
    merge(state.list, value)
  },
  remove(state, {car}) {
    state.list.splice(state.list.indexOf(car), 1)
  }, 
  setCar(state, car) { state.car = car }
  setCar(state, form) {
    let keys = Object.keys(form);
    keys.forEach((key) => state[key] = form[key])
  },
}

export const actions = {
  async get({commit}) {
    await this.$axios.get('cars')
      .then((res) => {
        if (res.status === 200) {
          commit('set', res.data)
        }
      })
  },
  async show({commit}, params) {
    await this.$axios.get(`cars/${params.car_id}`)
      .then((res) => {
        if (res.status === 200) {
          commit('setCar', res.data)
        }
      })
  },
  async set({commit}, car) {
    await commit('set', car)
  }
}
```  

```javascript
// Component
computed: {
  ...mapState({
    cars: state => state.cars.list,
    car: state => state.cars.car
  })
  blueCars: {
    get() { return this.$getters.blueCars },
    set(car) { this.$store.dispatch('cars/paintBlue', car)
  }
},
methods: {
  ...mapMutations({
    setCars: 'cars/set'
  }),
},
created(){
  // instead of this.$store.commit('cars/set', val)
  this.setCars([{id: 1, model: "Tacoma", brand: "Toyota"}])
}
```

### asyncData Method
https://nuxtjs.org/guide/async-data

asyncData will be called automatically.
asyncData is called before Vue rendering, therefore, it has NOT this.
asyncData receives CONTEXT as its argument. Context gives asyncData access to context.params, context.query, context.store and context.error()
The result asyncData returns will be AUTOMATICALLY MERGED with Vue data. This means property defined in asyncData can be displayed in template directly. 

```vue
<template>
  <h1>{{ title }}</h1>
</template>

<script>
import axios from 'axios'
export default {
  // Option 1: Returning a Promise
  asyncData (context) {
    const { app, store, route, params, query, env, isDev, isHMR, redirect, error} = context
    return axios.get(`https://my-api/posts/${params.id}`)
      .then((res) => {
        return { title: res.data.title }
      })
      // Handling Errors
      .catch((e) => {
        error({ statusCode: 404, message: 'Post not found' })
      })
  },
  // Option 2: Using async/await
  async asyncData ({ params }) {
    const { data } = await axios.get(`https://my-api/posts/${params.id}`)
    return { title: data.title }
  }
}
</script>
```

### Listening to query changes
for example when building a pagination component
https://nuxtjs.org/api/pages-watchquery

### retrieve initial data
In Vue, we retrieve initial data using the created hook. 
In Nuxt, we have two options: the fetch method and the nuxtServerInit method. 
Both these two methods will be invoked automatically. But they will be invoked at a different timing.

### FETCH FUNCTION
- Context.params and context.query give the fetch method access to data passed in URL. They allow the fetch to act based on data passed by URL.
- Context.error() enables the fetch method to trigger the error page should something goes wrong.
- Context.store.commit() allows the fetch method to trigger VueX mutation method which can set VueX state properties.

The fetch method is used to fill the store before rendering the page, 
it's like the asyncData method except it doesn't set the component data.

```vue
<template>
  <h1>Stars: {{ $store.state.stars }}</h1>
</template>

<script>
export default {
  fetch ({ store, params }) {
    return axios.get('http://my-api/stars')
    .then((res) => {
      store.commit('setStars', res.data)
    })
  }
}
</script>
```

NuxtServerInit() is a VueX action method  
So nuxtServerInit has two contexts. The first one is the VueX context and the second one the Nuxt context

## LOCALSTORAGE METHODS
- setItem()	Add key and value to local storage
- getItem()	Retrieve a value by the key
- removeItem()	Remove an item by key
- clear()	Clear all storage

Test out what's in local storage by going to the javascript console  
```javascript
localStorage
// Output: Storage {key: "value", length: 1}
```

## ROUTER (nuxt.config.js)
```javascript
  router: {
    middleware: 'stats',
    extendRoutes(routes, resolve) {
      let index = routes.findIndex(route => route.name === 'main')
      routes[index] = {
        ...routes[index],
        components: {
          default: routes[index].component,
          top: resolve(__dirname, 'components/mainTop.vue')
        },
        chunkNames: {
          top: 'components/mainTop'
        }
      }
    }
  },
```

```javascript
// By default, Nuxt.js scrolls to the top when you go to another page, but with children routes, Nuxt.js keeps the scroll position. 
<script>
export default {
  scrollToTop: true // (default: false)
}
</script>
```

## MIDDLEWARE  
In the Nuxt lifecycle, middleware is invoked before the validate method.

Context.params and context.query can get us values passed in URL.
Context.store gets us VueX.
Context.error() can help us trigger error page and send value to the error page.

Middleware can be bond to a specific page (component) or all pages (nuxt.config.js)

## LAYOUT
Whether you want to include a sidebar or having distinct layouts for mobile and desktop

**Default Layout**
layouts/default.vue
It will be used for all pages that don't have a layout specified.

**Custom Layout**

```vue
// layouts/blog.vue
<template>
  <div>
    <div>My blog navigation bar here</div>
    <nuxt/>
  </div>
</template>

// pages/posts.vue
<script>
export default {
  layout: 'blog'
}
</script>
```
**Error Page**
layouts/error.vue

```vue
<template>
  <div class="container">
    <h1 v-if="error.statusCode === 404">Page not found</h1>
    <h1 v-else>An error occurred</h1>
    <nuxt-link to="/">Home page</nuxt-link>
  </div>
</template>

<script>
export default {
  name: 'NuxtError',
  props: {
    error: {
      type: Object,
      default: null
    }
  },
}
</script>
```