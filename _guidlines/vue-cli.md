
## INSTALLATION
https://cli.vuejs.org/guide/webpack.html  
https://cli.vuejs.org/guide/css.html  
https://cli.vuejs.org/guide/cli-service.html  

  npm install -g @vue/cli  
  npm install -g @vue/cli-service-global  
  vue --version  

  ```
  vue serve -o MyComponent.vue
  ```  
  -o, --open  Open browser  
  -c, --copy  Copy local url to clipboard  
  -h, --help  output usage information  

  ```
  vue build MyComponent.vue
  ```  
  -t, --target <target>  Build target (app | lib | wc | wc-async, default: app)  
  -n, --name <name>      name for lib or web-component (default: entry filename)  
  -d, --dest <dir>       output directory (default: dist)  
  -h, --help             output usage information  

  ```
  vue create hello-world
  ```  

  ```
  vue ui
  ```  
## Installing Plugins in an Existing Project
  ```
  vue add @vue/eslint
  vue add router
  vue add vuex
  ```  
  


## Vue cli serve
  ```  
  vue-cli-service serve [options] [entry]
  ```  
  --open    open browser on server start  
  --copy    copy url to clipboard on server start  
  --mode    specify env mode (default: development)  
  --host    specify host (default: 0.0.0.0)  
  --port    specify port (default: 8080)  
  --https   use https (default: false) 

  ```   
  vue-cli-service build --modern  
  ```
  Vue CLI will produce 2 versions of your app: 
  `<script nomodule>`: one modern bundle targeting modern browsers that support ES modules
  `<script type="module">`: one legacy bundle targeting older browsers


HTML
`<%= VALUE %>` for unescaped interpolation;
`<%- VALUE %>` for HTML-escaped interpolation;
`<% expression %>` for JavaScript control flows.

```HTML
  <link rel="preload">
  <link rel="prefetch">
  <!-- tells the browser to prefetch content that the user may visit in the near future in the browser's idle time, after the page finishes loading -->
```
## CONFIG
https://cli.vuejs.org/config/
- .vuerc file: global cli config
  ```JAVASCRIPT
  // .vuerc file
  {
    "vuePlugins": {
      "service": ["my-commands.js"],
      "ui": ["my-ui.js"]
    }
  }
  ```
- vue.config.js (in your project root, next to package.json)  
  is an optional config file that will be automatically loaded by @vue/cli-service
- You can also use the vue field in package.json, but do note in that case you will be limited to JSON-compatible values only.


### Static assets
Static assets can be handled in 2 different ways:
1. Imported in JavaScript or referenced in templates/CSS via relative paths. Such references will be handled by webpack.
2. Placed in the public directory and referenced via absolute paths. These assets will simply be copied and not go through webpack.  
  => don't worry about browsers caching their old versions.

### URL Transform Rules
absolute path (e.g. /images/foo.png) => be preserved as it is.
starts with ., it's interpreted as a relative module request and resolved based on the folder structure on your file system.
starts with ~, anything after it is interpreted as a module request. This means you can even reference assets inside **node modules**:
starts with @, it's also interpreted as a module request. This is useful because Vue CLI by default aliases @ to <projectRoot>/src. (templates only)

- The public Folder
Any static assets placed in the public folder will simply be copied and not go through webpack. 
You need to reference them using absolute paths.  
If your app is not deployed at the root of a domain, you will need to prefix your URLs with the publicPath

```vue
<link rel="stylesheet" type="text/css" href="<%= BASE_URL %>/css/bootstrap.min.css">
<style src="../node_modules/vue-multiselect/dist/vue-multiselect.min.css"></style>

<script>
import '../public/style.scss'
</script>

<style lang="scss">
  @import '~/assets/css/master.css';
  @import '../public/assets/css/guildyou.scss';
  @import '~vue-multiselect/dist/vue-multiselect.min.css'
</style>


data () {
  return {
    publicPath: process.env.BASE_URL
  }
}

<img :src="`${publicPath}my-image.png`">
```
## COMMON PLUGINS
  ```
  npm install -D sass-loader node-sass
  ```  

  Update plugins:  
  ```
  npm outdated
  npm update
  npm update @vue/cli-plugin-babel
  ```


