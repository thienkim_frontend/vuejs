import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Staff from './views/Staff.vue'
import StaffDetail from './views/StaffDetail.vue'
import StaffList from './views/StaffList.vue'
import Login from './views/Login.vue'
import Admin from './views/Admin.vue'
import NotFound from './views/NotFound.vue'
import NetworkIssue from './views/NetworkIssue.vue'
import NProgress from 'nprogress'
import store from '@/store'

Vue.use(Router)

const scrollBehavior = function (to, from, savedPosition) {
  if (savedPosition) {
    // savedPosition is only available for popstate navigations (triggered by the browser's back/forward buttons)
    return savedPosition
  } else {
    const position = {}

    if (to.hash) {
      position.selector = to.hash
      if (to.hash === '#json-data') {
        position.offset = { y: 100 }
      }
      if (document.querySelector(to.hash)) {
        return position
      }
      return false
    }

    return new Promise(resolve => {
      if (to.matched.some(m => m.meta.scrollToTop)) {
        position.x = 0
        position.y = 0
      }
      this.app.$root.$once('triggerScroll', () => {
        resolve(position)
      })
    })
  }
}

export const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    props: true,
    meta: { 
      scrollToTop: true,
    }
  },
  {
    path: '/staff/create',
    name: 'form',
    component: () => import('./views/Form.vue'),
    // component: () => ({
    //   component: import("./Tooltip"),
    //   loading: AwesomeSpinner,
    //   error: SadFaceComponent,
    //   delay: 500,
    //   timeout: 5000
    // })
    meta: { 
      title: "Create new staff",
      description: "This is a collection of examples to showcase the features of VeeValidate" 
    }
  },
  {
    path: '/staffs/all',
    name: 'staffs-all',
    component: Staff,
  },
  {
    path: '/staffs',
    name: 'staffs',
    component: StaffList,
    props: true,
  },
  {
    path: '/staffs/:id',
    name: 'staff-detail',
    component: StaffDetail,
    props: true,
    meta: { requiresAuth: false },
    beforeEnter(to, from, next){
      store
      .dispatch('event/fetchEvent', to.params.id)
      .then(event => {
        to.params.staff = event
        next()
      })
      .catch(error => {
        if(error.response && error.response.status == 404){
          next({name: '404', params: {resource: 'event'}})
        }else{
          next({name: 'network-issue'})
        }
      })
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/admin',
    name: 'admin',
    component: Admin,
    meta: { requiresAuth: true } 
  },
  {
    path: '/404',
    name: '404',
    component: NotFound,
    props: true
  },
  {
    path: '/network-issue',
    name: 'network-issue',
    component: NetworkIssue,
  },
]

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior
})

router.beforeEach((routeTo, routeFrom, next) => {
  NProgress.start()
  if (routeTo.matched.some(record => record.meta.requiresAuth)) {
    // requireAuth(routeTo, routeFrom, next)
    console.log('required author')
  } else {
    next() // make sure to always call next()!
  }
})

router.afterEach(() => {
  NProgress.done()
})

export default router;