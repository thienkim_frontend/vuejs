# ROUTER
Router examples	https://github.com/vuejs/vue-router/tree/dev/examples
Path-to-RegExp	https://github.com/pillarjs/path-to-regexp
Router API	https://router.vuejs.org/api/
History mode	https://router.vuejs.org/guide/essentials/history-mode.html

## ROUTER CONSTRUCTION OPTIONS
  - path
  - name
  - component
  - components: { [name: string]: Component } => for named views
  - redirect: String | name_route | Function
    Note that Navigation Guards are not applied on the route that redirects, only on its target.
  - alias: the URL remains 
  - meta
  - props: boolean | Object | Function => the route.params will be set as the component props
  - children: [] => for nested routes
  - beforeEnter: (to, from, next) => {}
  - caseSensitive: boolean; // use case sensitive match? (default: false)
  - pathToRegexpOptions: Object; // path-to-regexp options for compiling regex

## LAZY LOADING ROUTES
  - Code splitting: is one of the most compelling features of webpack. 
  This feature allows you to split your code into various bundles which can then be loaded on demand or in parallel. 

  - To group all the components nested under the same route into the same async chunkby  
    => use named chunks by providing a chunk name using a special comment syntax
```javascript
  // define an async component that will be automatically code-split by webpack
  const Bar = () => import(/* webpackChunkName: "group-foo" */ './Bar.vue')
  const Foo = () => import(/* webpackChunkName: "group-foo" */ './Foo.vue')
```
const routes = [
  { path: '/', component: Home,},
  { path: '*', component: Notfound },
  {
    path: '/settings',
    component: UserSettings,
    children: [{
        path: 'emails',
        component: UserEmailsSubscriptions
      }, {
        path: 'profile',
        components: {
          default: UserProfile,
          helper: UserProfilePreview
        }
      }],
    beforeEnter: (to, from, next) => { next() },
    meta: { scrollToTop: true }
  }, { 
    /// make part of the path optional by wrapping with parens and add "?"
    path: '(/foo)?/bar', component: Bar , alias: '/b', props: true
  }, 
  { // this route will only be matched if :id is all numbers
    path: '/user/:id(\\d+)',
    components: {
      default: User,
      post: UserPost
    },
    // for routes with named views, you have to define the `props` option for each named view:
    props: {default: true, post: false},
    children: [
      // UserHome will be rendered inside User's <router-view>
      // when /user/:id is matched
      { path: '', name: 'user', component: UserHome },

      // UserProfile will be rendered inside User's <router-view>
      // when /user/:id/profile is matched
      { 
        path: 'profile', 
        component: UserProfile, 
        meta: { requiresAuth: true } 
      }
    ]
  }, 
  { path: '/dynamic-redirect/:id?',
    redirect: to => {
      const { hash, params, query } = to
      if (query.to === 'foo') {
        return { path: '/foo', query: null }
      }
      if (hash === '#baz') {
        return { name: 'baz', hash: '' }
      }
      if (params.id) {
        return '/with-params/:id'
      } else {
        return '/bar'
      }
    }
  },
  { path: '/search', component: SearchUser, props: (route) => ({ query: route.query.q }) }
]
## SCROLL BEHAVIOR
- only available in html5 history mode
- defaults to no scroll behavior
- return false to prevent scroll
- return a scroll position object { selector: string, offset? : { x: number, y: number }} (offset only supported in 2.6.0+)
```javascript
const scrollBehavior = function (to, from, savedPosition) {
  if (savedPosition) {
    // savedPosition is only available for popstate navigations (triggered by the browser's back/forward buttons)
    return savedPosition
  } else {
    const position = {}

    // scroll to anchor by returning the selector
    if (to.hash) {
      position.selector = to.hash

      // specify offset of the element
      if (to.hash === '#anchor2') {
        position.offset = { y: 100 }
      }

      if (document.querySelector(to.hash)) {
        return position
      }

      // if the returned position is falsy or an empty object, will retain current scroll position.
      return false
    }

    return new Promise(resolve => {
      // check if any matched route config has meta that requires scrolling to top
      if (to.matched.some(m => m.meta.scrollToTop)) {
        // coords will be used if no selector is provided,
        // or if the selector didn't match any element.
        position.x = 0
        position.y = 0
      }

      // wait for the out transition to complete (if necessary)
      this.app.$root.$once('triggerScroll', () => {
        // if the resolved position is falsy or an empty object,
        // will retain current scroll position.
        resolve(position)
      })
    })
  }
}
```
## ROUTER CONSTRUCTION OPTIONS
  - hash (in browser): uses the URL hash for routing. Works in all Vue-supported browsers, including those that do not support HTML5 History API.
  - history: requires HTML5 History API and server config. 
  - abstract (in Node.js): works in all JavaScript environments, e.g. server-side with Node.js. 
    The router will automatically be forced into this mode if no browser API is present.
```javascript
// Create the router instance and pass the `routes` option
const router = new VueRouter({
  mode: 'history', // "hash" is the value default
  base: __dirname,
  routes, // short for `routes: routes` (array)
  linkActiveClass: "",
  scrollBehavior // function
})
```
## NAVIGATION GUARDS
  - Global Before Guards: router.beforeEach((to, from, next) => {}) 
  - Global Resolve Guards: router.beforeResolve((to, from, next) => {}) 
  - Global After Hooks: router.afterEach((to, from) => {}) 
  - Per-Route Guard directly on a route's configuration object: beforeEnter: ((to, from, ) => {})

  NOTE: BEFOREENTER IS ONLY CALLED WHEN THE COMPONENT IS CREATED  
  + to (route objects): the route being navigated to
  + from (route objects): the current route
  + next(): the function that must be called to resolve the hook
  + next(false): abort the current navigation.
  + next({ path: '/' }) redirect to a different location
  + next(error) if the argument passed to next is an instance of Error, 
    the navigation will be aborted and the error will be passed to callbacks registered via router.onError()

## In-Component Guards
1. beforeRouteEnter (to, from, next) {},
  called before component is created
  does NOT have access to this
  However, you can access the instance by passing a callback to next

2. beforeRouteUpdate (to, from, next) {},
  called when the route changes, but still using the same component, eg: `/foo/1` => `/foo/2`
  this is already available

3. beforeRouteLeave (to, from, next) {},
  Called when this component is navigated away from.
  Usage: prevent the user from accidentally leaving the route with unsaved edits

NOTE: ALL MUST CALL NEXT()

The Full Navigation Resolution Flow
  - Navigation triggered.
  - Call leave guards in deactivated components.
  - Call global beforeEach guards.
  - Call beforeRouteUpdate guards in reused components.
  - Call beforeEnter in route configs.
  - Resolve async route components.
  - Call beforeRouteEnter in activated components.
  - Call global beforeResolve guards.
  - Navigation confirmed.
  - Call global afterEach hooks.
  - DOM updates triggered.
  - Call callbacks passed to  in beforeRouteEnter guards with instantiated instances. 
  
```javascript
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!auth.loggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})
router.afterEach((to, from) => {})
router.beforeResolve((to, from, next) => {
  next(); 
})
```
## Router Instance Properties
  - router.app  
  - router.mode  
  - router.currentRoute  

## Router Instance Methods
  - router.beforeEach((to, from, next) => {})
  - router.beforeResolve((to, from, next) => {})
  - router.afterEach((to, from) => {})
  - router.push(location, onComplete?, onAbort?) // To navigate to a different URL
  - router.replace(location, onComplete?, onAbort?)
    onComplete: be called when the navigation successfully completed (after all async hooks are resolved)
    onAbort: be called when navigated to the same route, or to a different route before current navigation has finished
  - router.go(n: integer) // how many steps to go forwards or go backwards in the history stack
  - router.back()
  - router.forward()
  - router.resolve(location, current?, append?)
  - router.addRoutes(routes: Array<RouteConfig>) // Dynamically add more routes to the router.
  - router.onReady(callback, [errorCallback]) // This is useful in server-side rendering to ensure consistent output on both the server and the client
  - router.onError(callback) 
  - router.getMatchedComponents()
    Returns an Array of the components (definition/constructor, not instances) matched by the provided location or the current route. 
    This is mostly used during server-side rendering to perform data prefetching

Note: params are ignored if a path is provided, which is not the case for query
```javascript
const userId = '123'
router.push({ name: 'user', params: { id: 123 } }) // -> /user/123
router.push({ path: `/user/${userId}` }) // -> /user/123
router.push({ path: 'register', query: { plan: 'private' } }) // -> /register?plan=private

router.replace({ name: 'user'}, function(){
  const answer = window.confirm('Do you really want to leave? you have unsaved changes!')
    if (answer) {
      next()
    } else {
      next(false)
    }
})
```
## ROUTE OBJECT PROPERTIES
  The route object can be found in multiple places:
  - Inside components as this.$route
  - Inside $route watcher callbacks: watch:{ '$route' (to, from) {} }
  - As the return value of calling router.match(location)
  - Inside navigation guards as the first two arguments
  - Inside the scrollBehavior function as the first two arguments
    ```javascript
      scrollBehavior (to, from, savedPosition) {
        // `to` and `from` are both route objects
      }
    ```
  - $route.name: The name of the current route, if it has one
  - $route.redirectedFrom: The name of the route being redirected from, if there were one (redirect & alias)
  - $route.path: equals the path of the current route, always resolved as an absolute path. e.g. "/foo/bar"
  - $route.params
    - $route.params.pathMatch
  - $route.query: /foo?user=1 => $route.query.user == 1
  - $route.hash
  - $route.fullPath: including query and hash
  - $route.matched: An Array containing route records for all nested path segments of the current route.

## COMPONENT INJECTIONS
  - `this.$router`: The router instance
  - `this.$route`: The current active Route 

## Fetching Before Navigation: 
  Perform the data fetching  
  - in the beforeRouteEnter guard in the incoming component  
  - in the beforeRouteUpdate guard when route changes and this component is already rendered

## Fetching After Navigation
  Fetch data in the component's created hook

  To react to params changes in the same component (e.g. /users/1 -> /users/2)
  - Option1: you can simply watch the $route object
  - Option2: use the beforeRouteUpdate navigation guard
  
```javascript
export default {
  props: ['id'],
  data () {
    return {
      loading: false,
      post: null,
      error: null
    }
  },
  components: { UserSettingsNav },
  template: '<div> User is: {{$route.params.id + "; " + id}}</div>',
  beforeRouteEnter (to, from, next) { 
    getPost(to.params.id, (err, post) => {
      next(vm => vm.setData(err, post))
    }) 
  },
  beforeRouteUpdate(to, from, next) {
    this.post = null
    getPost(to.params.id, (err, post) => {
      this.setData(err, post)
      next()
    })
  },
  beforeRouteLeave (to, from, next) { 
    next()
  },
  created () {
    this.fetchData()
  },
  methods: {
    setData (err, post) {
      if (err) {
        this.error = err.toString()
      } else {
        this.post = post
      }
    },
    fetchData () {
      this.error = this.post = null
      this.loading = true
      getPost(this.$route.params.id, (err, post) => {
        this.loading = false
        this.setData(err, post)
      })
    },
    afterLeave () {
      this.$root.$emit('triggerScroll')
    },
    goBack() {
      // go back by one record, the same as history.back()
      window.history.length > 1 ? this.$router.go(-1) : this.$router.push('/')
    }
  },
  watch: {
    // To react to params changes in the same component (e.g. /users/1 -> /users/2), 
    // Option1: you can simply watch the $route object
    // Option2: use the beforeRouteUpdate navigation guard

    // in the parent component, watch the `$route` to determine the transition to use
    '$route' (to, from) {
      const toDepth = to.path.split('/').length
      const fromDepth = from.path.split('/').length
      this.transitionName = toDepth < fromDepth ? 'slide-right' : 'slide-left'
    }
  }
}
```

## TEMPLATE FOR ROUTER
1. `<router-link>` 
  automatically gets the `.router-link-active` class when its target route is matched
  - tag: will be rendered as an `<a>` tag by default
    `tag="li"`: `<a>` will get the correct href, but the active class will be applied to the outer `<li>` 
  - to: call router.push() internally
  - active-class: "router-link-active" (default)
  - replace: call router.replace() instead of router.push() when clicked, so the navigation will not leave a history record
  - append: appends the relative path to the current path
  - exact: false (default) => linkExactActiveClass: `"router-link-exact-active"`
2. `<router-view>`
  - name: to display multiple views at the same time instead of nesting them  
    It will render the component with the corresponding name in the matched route record's components  
    NOTE: With "named routes", if we need to change the URL path later, we only have to change it in one place

``` html
  <ul class="us__nav">
    <router-link to="/" exact>
    <router-link tag="li" to="/settings/emails"><a>emails</a></router-link><br>
    <!-- with query, resulting in `/register?plan=private` -->
    <router-link :to="{ path: 'register', query: { plan: 'private' }}">Register</router-link>
    <router-link 
      tag="li" 
      to="{ name: 'user', params: { userId: 123 }}"     
      replace       
      append        
    >
      <a>Links</a>
    </router-link>
  </ul>

  <transition-group :name="transitionName" mode="out-in" @after-leave="afterLeave">
    <keep-alive>
      <router-view name="profile" class ="us__content"></router-view>
      <router-view name="helper" class ="us__content"></router-view>
    </keep-alive>
  </transition-group>
```