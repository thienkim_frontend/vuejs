## VUEX
Update at: 15/01/2020
Version: vuex@2.0.0
Reference:
  https://vuex.vuejs.org/guide/  
  https://github.com/vuejs/vuex/tree/dev/examples/shopping-cart/store  
  
- [VUEX](#vuex)
  * [STORE](#store)
  * [STATE](#state)
  * [GETTERS](#getters)
  * [MUTATIONS](#mutations)
  * [ACTIONS](#actions)
- [MODULES](#modules)
- [DYNAMIC MODULE REGISTRATION](#dynamic-module-registration)
- [Two-way Computed Property](#two-way-computed-property)


### STORE
A "store" is basically a single object contains all your application level state. This also means usually you will have only one store for each application

There are two things that make a Vuex store different from a plain global object:
1. Vuex stores are reactive. When Vue components retrieve state from it, they will reactively and efficiently update if the store's state changes.
2. You cannot directly mutate the store's state. The only way to change a store's state is by explicitly committing mutations.  
  This ensures every state change leaves a track-able record, and enables tooling that helps us better understand our applications.

```javascript
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state:{
  // Current state of the application lies here.
  },
  getters:{
  // Compute derived state based on the current state. More like computed property.
  },
  mutations:{
  // Mutate the current state
  },
  actions:{
  // Get data from server and send that to mutations to mutate the current state
  }
})
```

### STATE
It's an object that can contain any type of information: strings, arrays or other objects.
It's information stored centrally throughout the app.

```javascript
export const state = () => ({
  // use a function for declaring module state to avoid stateful singletons in SSR
  foo: "bar"
})
```  

```javascript
// Getting Vuex State into Vue Components
import { mapState, mapActions, mapGetters } from 'vuex'

const Counter = {
  template: `<div>{{ count }}</div>`,
  computed: {
    // to map a state to a different name, use an object
    ...mapState('event', {
      staffs: state => state.events,
    }),
    ...mapState(['statuses']),
    ...mapGetters('event', ['getEventByStatus','getEventById']),
    count () {
      return this.$store.state.count
      // Whenever store.state.count changes, it will cause the computed property to re-evaluate, and trigger associated DOM updates.
    }
  }
}
```
### GETTERS 
  Getters are always read-only values  
  They don't change the state but they do format it so that we can use that information in the way we need it.
  Ex: it returns a new different object with the filtered data that you need
  However, they can be used in combination with computed properties and you could provide a custom setter

```javascript
export const getters = {
  sumWithRootCount(state, getters, rootState, rootGetters) {
    return state.count + rootState.count;
  },
  doneTodos: state => {
    return state.todos.filter(todo => todo.done)
  },
  doneTodosCount: (state, getters) => {
    return getters.doneTodos.length
  },
  // You can also pass arguments to getters by returning a function
  getTodoById: (state) => (id) => {
    return state.todos.find(todo => todo.id === id)
  }
}

<script>
import { mapState, mapMutations, mapActions, mapGetters } from 'vuex'
export default {
  name:'Profile',
  computed:{
    name: {
      // getter
      get: () => {
        return this.$store.name    
      },
      // setter
      set: (newValue) => {
        this.$store.dispatch('updateName',newValue)
      }
    },
    ...mapState(['name','age','dob'])
    ...mapGetters(['isAdult'])
  },
  methods:{
    ...mapMutations(['updateName']),
    ...mapActions(['getUserData'])
  }
}
</script>
```

### MUTATIONS  
- They are functions.
- The only way to actually change state in a Vuex store is by committing a mutation.
- You can pass an additional argument to store.commit, which is called the payload for the mutation
- Mutations Must Be **Synchronous**

```javascript
export const mutations = {
  incrementBy (state, payload) {
    state.count += payload.amount
  }
}

/* Invoke Mutation In Components
 * this.$store.commit('updateName','Alicia')
 * this.$store.commit({
    type: 'updateName',
    name: 'Alicia'
  })
*/
import { mapMutations } from 'vuex'
export default {
  methods: {
    ...mapMutations([
      // `mapMutations` also supports payloads:
      'incrementBy' // map `this.incrementBy(amount)` to `this.$store.commit('incrementBy', amount)`
    ]),
    ...mapMutations({
      add: 'increment' // map `this.add()` to `this.$store.commit('increment')`
    })
  }
}
```

### ACTIONS
- Actions commit mutations
- Actions can contain arbitrary **asynchronous** operations( calling API,...)
- Actions are triggered with the store.dispatch method
- Actions support the same payload format and object-style dispatch
- Action has 2 arguments context and payload

```javascript
getUserData(context,payload){
  let { state, commit, dispatch, getters, mutate, rootState, rootGetters } = context
  context.mutate(‘updateName’ , ‘Alicia’ ) 
}

```

```javascript
// modules/account.js file
export const actions = {
  async actionA ({ commit }) {
    commit('gotData', await getData())
  },
  incrementIfOddOnRootSum({ state, commit, dispatch, getters, rootState, rootGetters }, payload) {
    if ((state.count + rootState.count) % 2 === 1) {
      commit("increment");
    }
    getters.someGetter; // -> 'account/someGetter'
    rootGetters.someGetter; // -> 'someGetter'

    dispatch("someOtherAction", null, { root: true }); // -> root action: 'someOtherAction'
    return dispatch('someAction').then(() => { // -> 'account/someAction'
      commit('someOtherMutation')
    }) 
    // the returned value will be a Promise that resolves when all triggered handlers have been resolved.
  },
  // Register Global Action in Namespaced Modules
  someAction: {
    root: true,
    handler(namespacedContext, payload) {} // -> 'someAction'
  }
}

// In Components
import { mapActions } from 'vuex'
export default {
  methods: {
    ...mapActions([
      // `mapActions` also supports payloads:
      'incrementBy' // map `this.incrementBy(amount)` to `this.$store.dispatch('incrementBy', amount)`
    ]),
    ...mapActions({
      add: 'increment' // map `this.add()` to `this.$store.dispatch('increment')`
    })
  }
}
```

## MODULES  
- Vuex allows us to divide our store into modules. Each module can contain its own state, mutations, actions, getters, and even nested modules  
- Inside module actions, `context.state` will expose the local state, and root state will be exposed as `context.rootState`  

- `namespaced`: When the module is registered, all of its getters, actions and mutations will be automatically namespaced based on the path the module is registered at  
  By default, actions, mutations and getters inside modules are still registered under the global namespace - this allows multiple modules to react to the same mutation/action type.  
  Namespaced getters and actions will receive localized getters, dispatch and commit  

```javascript
const account = {
  namespaced: true,
  // nested modules
  modules: {
    // inherits the namespace from parent module
    myPage: {
      state: {},
      getters: {
        profile() {} // -> getters['account/profile']
      }
    },

    // further nest the namespace
    posts: {
      namespaced: true,
      state: {},
      getters: {
        popular() {} // -> getters['account/posts/popular']
      }
    }
  }
};

const store = new Vuex.Store({
  modules: {
    account
  }
})

```

```javascript
// COMPONENT
import { mapState, mapActions } from "vuex";
export default {
  computed: {
    ...mapState("some/nested/module", {
      a: state => state.a,
      b: state => state.b
    })
  },
  methods: {
    ...mapActions("some/nested/module", [
      "foo", // -> this.foo()
      "bar" // -> this.bar()
    ])
  }
};
```

## DYNAMIC MODULE REGISTRATION   
  `preserveState: true`: the module is registered, actions, mutations and getters are added to the store, but the state not.   
  It's assumed that your store state already contains state for that module and you don't want to overwrite it.  


```javascript
// register a module `myModule` after the store has been created
store.registerModule(
  "myModule",
  { preserveState: true }
);
// remove a dynamically registered module
store.unregisterModule(moduleName);
```

## Two-way Computed Property

```javascript
// <input v-model="message">
computed: {
  message: {
    get () {
      return this.$store.state.obj.message
    },
    set (value) {
      this.$store.commit('updateMessage', value)
    }
  }
}
// store
mutations: {
  updateMessage (state, message) {
    state.obj.message = message
  }
}
```