## ANIMATION

### Custom Transition Classes
  The classes are automatically prefixed with the name of the transition
  - v-enter, v-leave
  - v-enter-active, v-leave-active
  - v-enter-to (only 2.1.8+), v-leave-to

  Transition modes:  
  - in-out: New element transitions in first, then when complete, the current element transitions out.
  - out-in: Current element transitions out first, then when complete, the new element transitions in.

  Example: Use an external CSS library such as [animate.css](https://daneden.github.io/animate.css/)

```html
<template>
  <transition 
    name="fade" 
    mode="out-in"
    enter-active-class="animated tada" 
    leave-active-class="animated bounceOutRight" 
    :duration="{ enter: 500, leave: 800 }"
  >
  </transition>
</template>
<style scoped>
  .fade-enter-active{ transition: opacity 1.5s; }
  .fade-leave-active { opacity: 0; }
  .fade-enter, .fade-leave-to { opacity: 0; }
</style>
```
### JS Animation
  JavaScript Hooks: These hooks can be used in combination with CSS transitions/animations or on their own.  
  Plugin: 
  - [Velocity.js](https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.3/velocity.min.js): npm install velocity-animate -S
  - [TweenMax.js](https://greensock.com/docs/TweenMax): npm install gsap -S
  
```html
<transition
  v-on:before-enter="beforeEnter"
  v-on:enter="enter"
  v-on:leave="leave"
  v-bind:css="false" <!-- JavaScript-only transitions so that Vue can skip the CSS detection -->
></transtion>
```  
```javascript
methods: {
  mounted() {},
  beforeEnter(el) {
    TweenMax.set(el, {
      transformPerspective: 600,
      perspective: 300,
      transformStyle: "preserve-3d",
      autoAlpha: 1
    });
  },
  enter(el, done) { 
    done() // the done callbacks are required for the enter and leave hooks
  }
  leave(el, done) {
    done()
  }
}
```

### Transitions on Initial Render with "appear" attribute  
```html
<!-- Transitions on Initial Render  -->
<transition
  appear
  v-on:before-appear="customBeforeAppearHook"
  v-on:appear="customAppearHook"
  v-on:after-appear="customAfterAppearHook"
  v-on:appear-cancelled="customAppearCancelledHook"
>
</transition>
```

### Transitioning Between Elements
  If they have the same tag name, you must tell Vue that they are distinct elements by giving them unique key attributes

```html
<transition>
  <button v-bind:key="isEditing">
    {{ isEditing ? 'Save' : 'Edit' }}
  </button>
</transition>
```

### List Transitions
  The `v-move` class will be added when items are changing positions

```html
<transition-group tag="ul" name="flip-list">
  <li v-for="item in items" :key="item.id">
    {{ item.text }}
  </li>
</transition-group>
<style>
  .flip-list-move { transition: transform 1s; }
</style>

```

### Reusable Transitions
```javascript
Vue.component('my-special-transition', {
  template: '\
    <transition\
      name="very-special-transition"\
      mode="out-in"\
      v-on:before-enter="beforeEnter"\
      v-on:after-enter="afterEnter"\
    >\
      <slot></slot>\
    </transition>\
  ',
  methods: {
    beforeEnter: function (el) {},
    afterEnter: function (el) {}
  }
})
```