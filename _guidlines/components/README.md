
TABLE OF CONTENTS
- [INTRODUCTION](#introduction)
- [COMPONENTS SLOTS](#components-slots)
- [GLOBAL VS LOCAL COMPONENTS](#global-vs-local-components)
- [COMMUNICATION BETWEEN COMPONENTS](#communication-between-components)
- [COMPONENTS OPTIONS](#components-options)
  * [`data` option](#-data--option)
  * [`inheritAttrs` option](#-inheritattrs--option)
  * [Computed Properties VS Watchers](#computed-properties-vs-watchers)
  * [`props` options](#-props--options)
  * [`components` option](#-components--option)
  * [`directives` option](#-directives--option)
  * [`provide` & `inject` options](#-provide-----inject--options)
  * [`methods` options](#-methods--options)
  * [`filter` option](#-filter--option)
  * [`model` option](#-model--option)
- [COMPONENT LIFECYCLE HOOKS](#component-lifecycle-hooks)
- [Listening to Children Hooks](#listening-to-children-hooks)
- [Acessing Child Component Instances & Child Elements by `ref` attribute](#acessing-child-component-instances---child-elements-by--ref--attribute)
- [Dependency injection](#dependency-injection)
- [REUSING LOGIC IN VUE COMPONENTS: 3 ways](#reusing-logic-in-vue-components--3-ways)
- [BUILD-IN COMPONENTS](#build-in-components)
  * [`<keep-alive>`](#--keep-alive--)

## INTRODUCTION
Build-in instance properties and methods are prefixed with `$`to differentiate them from user-defined properties.
Object.freeze(), which prevents existing properties from being changed

```javascript
import BurgerComponent from './BurgerComponent.vue';
import {
  BurgerBun,
  SesameSeeds,
  Mayonaise,
} as Ingredients from './ingredients';

export default {
  components: {
    BurgerComponent,
    ...Ingredients
  },
}
```

## COMPONENTS SLOTS
  to serve as distribution outlets for content
  Slots can contain any template code, including HTML Or even other components
  
  Note: The slot and slot-scope attributes will continue to be supported in all future 2.x releases, 
  but are officially deprecated and will eventually be removed in Vue 3.

```html
<page-layout>
  <template v-slot="{ user = { firstName: 'Guest' } }"></template>
  <!-- use ES2015 destructuring to pull out specific slot props --> 

  <p> A paragraph for the main content </p>

  <template #footer> </template>
</page-layout>
```

```javascript
// page-layout.vue
<template>
  <slot>fallback (i.e. default) content for a slot, to be rendered only when no content is provided.</slot>
  <slot name="footer"></slot>
</template>
<script>
  computed: {
    inputIsComponent () {
      return (this.$slots.default && this.$slots.default.length > 0) && !!this.$slots.default[0].componentInstance
    },
  }
</script>
```

## GLOBAL VS LOCAL COMPONENTS
```javascript
// Local component
let PlanPickerComponent = {
  template: '',
  data(){
    return{
      plans: ['The Single', 'The curious']
    }
  }
}
new Vue({
  el: '#app',
  components: {
    'plan-picker': PlanPickerComponent
  }
})
```

```javascript
// Global component
Vue.component('plan-picker', {
  template: '',
  data(){
    return{
      plans: ['The Single', 'The curious']
    }
  }
})
```
## COMMUNICATION BETWEEN COMPONENTS
  Pass data from the child to the parent  
  NOTE: custom events are only allowed on one child-to-parent level.

```html  
  <label> 
    {{ label }}
    <input
      v-bind="$attrs"
      v-bind:value="value"
      v-on:input="$emit('input', inputListeners)"
      v-focus
    >
  </label>
```
## COMPONENTS OPTIONS
### `data` option
  must be declared as a function that returns the initial data object, 
  so that each instance can maintain an independent copy of the returned data object 

  ```javascript
    data () {
      return {
        count: this.initCounter
      }
    },
  ```

### `inheritAttrs` option
  you can manually decide which element you want to forward attributes to

  ```javascript
    // Disabling Attribute Inheritance
    // If you do not want the root element of a component to inherit attributes by default
    inheritAttrs: false 
  ```

### Computed Properties VS Watchers
  - A computed property will only re-evaluate when some of its reactive dependencies have changed => computed properties are cached  
    **Usage**: to perform asynchronous or expensive operations in response to changing data.
  - Watch: A method invocation will always run the function whenever a re-render happens.
  - Methods: are good for small, synchronous calculations

**Note: You should not use an arrow function to define a watcher**  
Arrow functions bind the parent context, so this will not be the Vue instance as you expect and this.updateAutocomplete will be undefined.  

```javascript 
  computed: {
    fullName: {
      get () {
        return this.firstName + ' ' + this.lastName
      },
      set (newValue) {
        var names = newValue.split(' ')
        this.firstName = names[0]
        this.lastName = names[names.length - 1]
      }
    },
    inputListeners(){
      var vm = this;
      return Object.assign({}, this.$listeners, { 
        input(event){ 
          vm.$emit('input', vm.$emit('input', event.target.value))
        }
      })
    }
  },
  watch: {
    c: {
      handler: function (val, oldVal) { },
      deep: true // the callback will be called whenever any of the watched object properties change regardless of their nested depth
    },

    d: {
      handler: 'someMethod',
      immediate: true // the callback will be called immediately after the start of the observation
    },
  }
```

### `props` options
- Allowed type values are:
  * Object - Only allows objects, such as :myProp=”{key: ‘value’}”.
  * Array - Only allows arrays, such as :myProp=”[1, 2, 3]”.
  * Function Only allows functions to be passed, such as *:myProp=”function(test) { return test.toUpperCase() }”.
  * String - Only allows strings, such as :myProp=”‘Example’” (or more simply, myProp=”Example”).
  * Number - Only allows numerical values, such as :myProp=”103.4”.
  * Boolean - Only allows true or false values, such as :myProp=”true”.
  * [Any Constructor Function] - You can pass in classes or constructor functions as well, and it will allow props that are instances of those functions.
  **Note: Vue.js component with Constructor**  
  https://inventi.studio/en/blog/vuejs-with-constructor-pattern  

- When prop validation fails, Vue will produce a console warning (if using the development build).
```javascript
import R from "ramda"

class Car {
  constructor(car = {}) {
    this.brand = R.is(String, car.brand) ? car.brand : ""
    this.model = R.is(String, car.model) ? car.model : ""
  }
  
  get name() { 
    return this.brand.concat(" ", this.model)
  }
  
  // return an Object that is ready to be send to API endpoint, when we need to create a new Car
  toCreatePayload() {
    return {
      brand: this.brand,
      model: this.model
    }
  }
}
```

```javascript
import Car from "constructors/Car"

function Person (firstName, lastName) {
  this.firstName = firstName
  this.lastName = lastName
}
export default{
  data: () => ({
    car: new Car()
  }),
  props: {
    author: Person,
    initCounter: { 
      type: Number, // Number, String, Object, Array, Boolean, Function, Promise
      required: true, 
      default: 0
    },
    propB: [String, Number], // Multiple possible types
    // Custom validator function
    propF: {
      // A validator returns a boolean, true if it is valid, false otherwise
      validator: function (value) {
        // The value must match one of these strings
        return ['success', 'warning', 'danger'].indexOf(value) !== -1
      }
    }
  },
  methods: {
    async createCar() {
      await ApiService.post("/api/cars", this.car.toCreatePayload())
      // Reset form after Car has been created
      Object.assign(this.car, new Car())
    }
  }
}  
```

### `components` option  
  **Error: Failed to mount component: template or render function not defined.**
  it needs A, but first A needs B, but B needs A, but A needs B, etc  
  => Resolved by “A needs B eventually, but there’s no need to resolve B first.”

```javascript  
  // Solution 01
  beforeCreate: function () {
  	this.$options.components.TreeFolderContents = require('./tree-folder-contents.vue').default
  }
  // Solution 02
  components: {
    TreeFolderContents: () => import('./tree-folder-contents.vue')
  }
```

### `directives` option

```javascript  
  // register a new custom directive locally
  directives:{
    focus: {
      inserted(el){ el.focus; }
    }
  },
```

### `provide` & `inject` options
```javascript  
  provide(){}, // for parent component
  inject(){},  // for children component
```

### `methods` options
```javascript  
  methods: {
    addToCart () {
      this.$emit('add-to-cart')
    },
  }
```

### `filter` option
  - Filters are usable in two places: mustache interpolations and v-bind expressions 
  - Filters can be chained
  - Filters can take as many arguments as you need

```html
<template>
  <!-- in mustaches -->
  {{ message | capitalize }}

  <!-- in v-bind -->
  <div v-bind:id="rawId | formatId"></div> 
</template>
```

```javascript  
  // Global Filters
  Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
  })

  // Local Filters 
  filters: {
    currencydecimal (value) {
      return value.toFixed(2)
    }
  },
```

### `model` option
  **How to add v-model to custom components**
  Tutorial: https://alligator.io/vuejs/add-v-model-support/
  By default, v-model on a component uses `value` as the prop and `input` as the event

```html
<basic-input v-model="email" />
<!-- <basic-input :hidden="email" @blur="e => email = e.target.value" /> -->
```
```javascript 
// BasicInput.vue
<template>
  <input @input="handleInput" />
</template>
export default {
  prop: ['hidden'],
  model: {
    prop: 'hidden',
    event: 'blur'
  }
  methods: {
    handleInput (value) {
        this.$emit('blur', value)
    }
  }
}

```

## COMPONENT LIFECYCLE HOOKS
  giving users the opportunity to add their own code at specific stages

  **Note: Don’t use arrow functions on an options property or callback.**  
    Since arrow functions are bound to the parent context, this will not be the Vue instance as you’d expect, often resulting in errors such as  
  - Uncaught TypeError: Cannot read property of undefined
  - Uncaught TypeError: this.myMethod is not a function.

```javascript  
  created() {
    // to fetch some data for your component on initialization
  }, 
  mounted() {
    // to access or modify the DOM of your component immediately before or after the initial render
    const picker = new Pickaday()

    this.$once("hook:beforeDestroy", () => {
      picker.destroy()
    })

    // This hook is not called during server-side rendering.
    this.$nextTick(function () {
      // Code that will run only after the entire view has been rendered
    })
  },
  updated() {},
  destroyed() {}
```

## Listening to Children Hooks
```html  
<template>
  <Child @hook:mounted="childMounted" @hook:updated="doSomething"/>
</template>
```

## Acessing Child Component Instances & Child Elements by `ref` attribute
It will apply changes to the DOM directly, as any changes you apply may be overwritten if you are not careful.  
While you should be cautious with changing the DOM when using references, it is safer to do read-only operations such as reading values from the DOM.  

Acessing the Parent Component Instance by `$parent` property
Disadvantage: didn’t scale well to more deeply nested components

```javascript  
  this.inputElement = this.$refs['inputSlot'].querySelector('input')
  this.$parent.getMap
```

## Dependency injection
`provide` options allows us to specify the data/methods we want to provide to descendent components.
`inject` option to receive specific properties from its parent components 

Advantages:  
  - without fear that we might change/remove something that a child component is relying on
  - allows you to share state in all children components
  - will update when you change the value of the parent
  - doesn't require a full state management solution like vuex
  - good for a component library where you need to keep things synced between children

Disadvantages:  
  - if you allow children to set the value of the parent, you can cause some unexpected issues if things go out of order
  - It is not good practice to rely on injected values to be there.  
    It is a better pattern to be explicit about what is available to and needed by a component through props.
  - not recommended for most applications if you need to handle a lot of state based changes.

```javascript  
  provide: function () {
    return {
      getMap: this.getMap
    }
  },
  inject: ['getMap']
```
## REUSING LOGIC IN VUE COMPONENTS: 3 ways
[Tutorial link](https://vueschool.io/articles/vuejs-tutorials/reusing-logic-in-vue-components/)  
[Preview code - codesandbox.io](https://codesandbox.io/s/xl69pv1l5p)

1. Component inheritance: using the "extends" component option
  - Be aware that component extension is not as classes inheritance. 
  - In this case, Vue merges both the parent and child component options creating a new mixed object.
  - We can only inherit from one component
  - The child's hooks are executed before the parent's

2. Mixins: using the "mixins" component options
  - Mixins cannot have any template or style tags, they're just plain JavaScript.
  - We can combine the functionality of multiple of component.
  - Their hooks are executed before the component's using it
  
```javascript  
  // GLOBAL MIXIN
  // inject a handler for `myOption` custom option
  Vue.mixin({
    created: function () {
      var myOption = this.$options.myOption
      if (myOption) {
        console.log(myOption)
      }
    }
  })

  // Always use the $_ prefix for custom private properties in a plugin, mixin, etc
  // => avoid conflicts with code by other authors, also include a named scope
  var myGreatMixin = {
    methods: {
      $_myGreatMixin_update: function () {}
    }
  } 

  new Vue({ myOption: 'hello!' })
  Vue.config.optionMergeStrategies.myOption = function (toVal, fromVal) {
    // return mergedVal
  }
```

```javascript 
export default{
  mixins: [singleErrorExtractorMixin]
} 
```

3. Reusable components that just receive props and emit events

## BUILD-IN COMPONENTS
### `<keep-alive>`
  This build-in component is designed for the case where it has one direct child component that is being toggled
