# DIRECTIVE BASICS

### v-show 
  Only toggles the display CSS property of the element.

### v-if
  Remove/ insert the element
  - `v-if` is a directive, it has to be attached to a single element.
  - If we want to toggle more than one element,we can use v-if on a `<template>` element
  - A `v-else` element must immediately follow a `v-if` or a `v-else-if` element, otherwise it will not be recognized
  - If templates use the same elements such as the `<input>`, add a `key` attribute with unique values => Vuejs don’t re-use them

```html
<template v-if="loginType === 'username'">
  <label>Username</label>
  <input placeholder="Enter your username" key="username-input">
</template>
<template v-else>
  <label>Email</label>
  <input placeholder="Enter your email address" key="email-input">
</template>
```

### v-for  
  You need to provide a unique key attribute for each item

```html
<div v-for="(value, key, index) in object" :key="key">
  {{ index }}. {{ key }}: {{ value }}
</div>
```
### v-on   
  To attach event listeners that invoke methods on our Vue instances  
  It can access the original DOM event in an inline statement handler by $event variable

  - $emit('enlarge-text', 0.1): emitting a Value With an Event
  - $on(eventName, eventHandler):  Listen for an event
  - $once(eventName, eventHandler):  Listen for an event only once 
  - $off(eventName, eventHandler): Stop listening for an event

```html
<button v-on:click="warn('Form cannot be submitted yet.', $event)">Submit</button>
```
  - **Binding Native Events to Components**   
    $listeners property containing an object of listeners being used on the component,  
    You can forward all event listeners on the component to a specific child element with `v-on="$listeners"`

### v-once 
  Render the element and component once only.

### v-model 
  two-way data bindings on form input, textarea, and select elements 

| Modifiers | Description |
| ----------------- | -------- |
| v-model.number    | `input type="number"` always returns a string. If the value cannot be parsed with parseFloat(), then the original value is returned.
| v-model.trim      | whitespace from user input to be trimmed automatically,
| v-model.lazy      | listen to change events instead of input

```html
<select v-model="selected">
  <option v-for="option in options" v-bind:value="option.value">
    {{ option.text }}
  </option>
</select>
```
### v-slot

### v-cloak  
  hide un-compiled mustache bindings until the Vue instance is ready.  
  You can use as many as you want, it's just a css attribute selector.  
```css
[v-cloak] {display: none}
```

### v-html
  raw HTML => very dangerous because it can easily lead to XSS vulnerabilities  
  Only use `v-html` on trusted content and never on user-provided content.

### v-bind 
  to reactively update an HTML attribute
  - If you want to pass all the properties of an object as props, you can use v-bind without an argument  
    Ex: `v-bind` instead of `v-bind:prop-name`
  - `v-bind:class` We can pass an object, array to dynamically toggle classes. Existing classes on this element will not be overwritten.
  - `v-bind:style` can use either camelCase or kebab-case (use quotes with kebab-case) for the CSS property names  
  - `v-bind.sync`

```html
<div
  class="static"
  v-bind:class="{ active: isActive, 'text-danger': hasError }"
  v-bind:style="{ color: activeColor, fontSize: fontSize + 'px' }"
>
  <img :src="user.avatar_url"/>
</div>
```

## SPECIAL ATTRIBUTES
### `key`
  By add a key attribute with unique values, Vue will not re-use them.
### `ref`   
  is  Used for dynamic components and to work around limitations of in-DOM templates
### `is` 
  - only an `<li>` element is valid inside a `<ul>`
  - only an `<tr>` element is valid inside a `<table>`
  - Dynamic component

```html
<table>
  <tr is="my-row"></tr>
</table>

<!-- dynamic component -->
<component :is="view"></component>
```

### `tag`

```html
<transition-group tag="todo-layout-container"
  :props="someValue" 
  @event="someMethod()" 
  class="css-class"
>
``` 

### Event Modifiers
| Event Modifiers | Description |
| -------- | -------- |
| .stop       | propagation will be stopped
| .prevent    | will no longer reload the page
| .capture    | an event targeting an inner element is handled here before being handled by that element
| .self       | only trigger handler if event.target is the element itself i.e. not from a child element 
| .once
| .passive    | improving performance on mobile devices.

- **v-on:scroll.passive="onScroll"** the scroll event's default behavior (scrolling) will happen immediately, instead of waiting for `onScroll` to complete in case it contains `event.preventDefault()`
- **v-on:click.prevent.self** will prevent all clicks
- **v-on:click.self.prevent** will only prevent clicks on the element itself.
- **v-model.lazy** synced after "change" instead of "input"
- **v-on:submit.prevent** this will no longer reload the page on submission
- @click.once not to be confused with v-once, this click event will be triggered once.
- @mousemove.stop is comparable to e.stopPropogation()

### Key Modifiers
| Key Modifiers | Description |
| -------- | -------- |
| v-on:focus.native         | listen directly to a native event on the root element of a component
| v-on:keyup.enter="submit" | 
| v-on:keyup.13="submit"    | 
| @click.ctrl.exact         |

## HOOK FUNCTIONS FOR A DIRECTIVE
  - bind: called only once, when the directive is first bound to the element. This is where you can do one-time setup work.
  - inserted: called when the bound element has been inserted into its parent node 
  (this only guarantees parent node presence, not necessarily in-document).
  - update: called after the containing component’s VNode has updated, but possibly before its children have updated.  
  The directive’s value may or may not have changed, but you can skip unnecessary updates by comparing the binding’s current and old values   
  - componentUpdated: called after the containing component’s VNode and the VNodes of its children have updated.
  - unbind: called only once, when the directive is unbound from the element.

## Define TEMPLATES 
1. **`<my-component inline-template>`** : avoid to use
2. **template** option
3. **`<template>`** element
4. **`<script type="text/x-template">`** : needs to be defined outside the DOM element to which Vue is attached. => for demos purpose

## CUSTOM DIRECTIVES

```html
  <input v-focus v-demo="{ color: 'white', text: 'hello!' }">
```

```javascript
// Register a GLOBAL custom directive called `v-focus`
Vue.directive('focus', {
  //  called only once, when the directive is first bound to the element. Usage: one-time setup
  bind: function(el, binding, vnode){
      var s = JSON.stringify
      el.innerHTML =
      'name: '       + s(binding.name) + '<br>' +
      'value: '      + s(binding.value) + '<br>' +
      'expression: ' + s(binding.expression) + '<br>' +
      'argument: '   + s(binding.arg) + '<br>' +
      'modifiers: '  + s(binding.modifiers) + '<br>' +
      'vnode keys: ' + Object.keys(vnode).join(', ')
  }, 
  // When the bound element is inserted into the DOM...
  inserted: function (el) {
    // Focus the element
    el.focus()
  },
  // update: called after the containing component’s VNode has updated, but possibly before its children have updated
  update: function( oldVnode){},
  // componentUpdated: called after the containing component’s VNode and the VNodes of its children have updated
  componentUpdated: function(){},
  // unbind: called only once, when the directive is unbound from the element.
  unbind: function(){}
})
```
```javascript
// Function Shorthand: the same behavior on bind and update
Vue.directive('color-swatch', function (el, binding) {
    el.style.backgroundColor = binding.value
})
```

