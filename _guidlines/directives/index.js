/* https://css-tricks.com/power-custom-directives-vue/
 * <div v-example:arg.modifier="value"> 
 * value: function
 * argument: bottom | top
 * modifiers:
 * lifecycle hooks: 
 */

export const stickyDirective = {
  bind(el, binding, vnode) {
    const loc = binding.arg === "bottom" ? "bottom" : "top";
    const modifiers = binding.modifiers;

    if (!el) return;
    
    el.classList.add("is-fixed");
    // el.style[loc] = binding.value || '0' + 'px';

    if (loc === "bottom") {
      el.style.background = "burlywood";
    } else {
      el.classList.add("top-nav");
    }

    if (modifiers.underline) {
      el.style.textDecoration = "underline";
    }
    console.log("bind");
  },
  inserted(el, binding, vndoe) {
    el.customFn = function(evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', el.customFn);
      }
    };
    window.addEventListener('scroll', el.customFn);
    console.log("inserted");
  },
  updated(el, binding, vnode) {
    console.log("updated");
  },
  componentUpdated(el, binding, vnode, oldVnode) {
    console.log("componentUpdated", el.customFn);
  },
  unbind(el, binding, vnode) {
    window.removeEventListener('scroll', el.customFn);
    console.log("unbinded");
  }
}

/* Validators
 * <div v-example:arg.modifier="value"> 
 */
export function isNameJoe(value) {
  if (!value) return true;
  return value === "Joe";
}

export function notGmail(value = "") {
  return !value.includes("gmail");
}

export function isEmailAvailable(value) {
  if (value === "") return true;

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(value.length > 10);
    }, 500);
  });
}