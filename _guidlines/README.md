## TABLE OF CONTENTS
* [Vue cli](./vue-cli.md)
* [Components](./components/)
* [Directives](./directives/)  
  Directives are special attributes with the `v-` prefix  
  Modifiers are special postfixes denoted by a dot, which indicate that a directive should be bound in some special wa* EX: v-on:submit.prevent  
  Shorthands for ONLY 2 directives: `v-bind` and `v-on`   
    v-bind:href="" => :href=""  
    v-on:click="" => @click=""  
* [Router](./routers/)
* [Animation](./animation.md)
* [Vuex](./vuex/)
* [Nuxt](./nuxt.md)
* [Plugins](./plugins/)


## Environment Variables
### Option1: You can specify env variables by placing the following files in your project root  

```
.env                # loaded in all cases
.env.local          # loaded in all cases, ignored by git
.env.[mode]         # only loaded in specified mode
.env.[mode].local   # only loaded in specified mode, ignored by git
```

```javascript
// An env file simply contains key=value pairs of environment variables
VUE_APP_I18N_LOCALE=jp
```

### Option 2: create an application that already supports environment files
$ npx vue-cli init webpack my-app

```
config/
----- index.js      
----- dev.env.js      // when you are running the application through the npm run dev command, the dev.env.js file is used
----- prod.env.js     // when you compile the project for production with the npm run build command, the prod.env.js file is used instead
```

```javascript
// dev.env.js
'use strict'
var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_URL: '"http://localhost:8000/api/v1"'
})

// prod.env.js
module.exports = {
  NODE_ENV: '"production"',
  BASE_URL: '"https://my-prod-api/api/v1"'
}

// yourComponent.vue
<script>
export default {
  mounted () {
    let url = process.env.BASE_URL + '/items'
    this.$http.get(url)
    .then((response) => {
      console.log('items', response)
    }, (error) => {
      console.log('error', error)
    })
  }
}
</script>
```

## VUE API
- Vue.version
- Vue.use(plugin)
- Vue.component(): Register or retrieve a global component
- Vue.filter(): Register or retrieve a global filter.
- Vue.set( target, key, value ): add new properties to reactive objects
- Vue.extend( options ): Create a “subclass” of the base Vue constructor

## Instance Properties
$ is a convention Vue uses for properties that are available to all instances. 
This avoids conflicts with any defined data, computed properties, or methods.  
- vm.$data
- vm.$props
- vm.$options
- vm.$parent
- vm.$root
- vm.$el: return The root DOM element that the Vue instance is managing.  

```javascript
let form = this.$el.closest('form')
```

- vm.$children
- vm.$slots
- vm.$refs
- vm.$isServer
- vm.$attrs
- vm.$listeners

- vm.$watch( expOrFn, callback, [options] )
- vm.$set( target, key, value )
- vm.$on( event, callback )
- vm.$once( event, callback )
- vm.$off( [event, callback] )
- vm.$emit( eventName, […args] )

## STYLE
For applications, styles in a top-level App component and in layout components may be global, but all other components should always be scoped

```css
<style lang="scss" scoped></style>
```

## COMMON PLUGINS
  $ npm install vuejs-datepicker --save-dev
  $ npm install gsap --save-dev
  $ npm install velocity-animate --save-dev

### [Lodash](https://lodash.com/docs/4.17.11)
  $ npm install lodash --save-dev  

```javascript
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'

const requireComponent = require.context(
  './components', // The relative path of the components folder
  false,          // Whether or not to look in subfolders
  /Base[A-Z]\w+\.(vue|js)$/ // The regular expression used to match base component filenames
)

requireComponent.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireComponent(fileName)

  // Get PascalCase name of component
  const componentName = upperFirst(
    camelCase(
      // Gets the file name regardless of folder depth
      fileName
      .split('/')
      .pop()
      .replace(/\.\w+$/, '')
    )
  )
```

### [VeeValidate](https://vuelidate.netlify.com/#sub-validation-groups)
  $ npm install vee-validate --save-dev  

### [Vuelidate](https://github.com/vuelidate/vuelidate)
  $ npm install vuelidate vuelidate-error-extractor --save-dev
```javascript
import Vuelidate from 'vuelidate'
import VuelidateErrorExtractor, { templates } from "vuelidate-error-extractor"

Vue.use(Vuelidate);
Vue.use(VuelidateErrorExtractor, {
  i18n: false,
  messages: {
    required: "{attribute} is required!",
    isEmailAvailable: "{attribute} is not available. Must be at least 10 characters long.",
    sameAsPassword: "Passwords must be identical."
  }
})
Vue.component("form-group", templates.singleErrorExtractor.foundation6)
Vue.component('FormWrapper', templates.FormWrapper)

// COMPONENTS
import {required, email} from 'vuelidate/lib/validators'
export default {
  validations: {
    form: {
      email: {
        required,
        email
      }
    }
  }
}
```

### nprogress
```javascript
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

NProgress.start()
NProgress.done()

```

### Sass
 $ npm install node-sass sass-loader --save-dev

### [vue-i18n](https://kazupon.github.io/vue-i18n/)
Usage: To integrates some localization features(ex: multi-language message) to your Vue.js Application

```javascript
import VueI18n from 'vue-i18n'

<p>{{ $t("message.hello") }}</p>
```
### [jqueryui-datepicker](https://api.jqueryui.com/datepicker/)  
Jquery vs Vue.js
Tutorial: https://vuejsdevelopers.com/2017/05/20/vue-js-safely-jquery-plugin/
- We can utilise lifecycle hooks for setup and teardown of the jQuery code
- We can use the component interface to communicate with the rest of the Vue app via props and events
- Components can opt-out of updates with v-once  
  v-once directive is used to cache a component in the case that it has a lot of static content. 

$ npm install jquery --save

```html
<!-- Pass config with props -->
<!-- With v-once directive, jQuery is going to have unhampered control over this element during the lifecycle of the app -->
<date-picker @update-date="updateDate" date-format="yy-mm-dd" v-once></date-picker>
```

```javascript
// vue.config.js
module.exports = {
  pluginOptions: {
    new webpack.ProvidePlugin({
      $: 'jquery',
      jquery: 'jquery',
      'window.jQuery': 'jquery',
      jQuery: 'jquery'
    })
  }
}
// package.json or .eslintrc.js
"eslintConfig": {
  globals: {
    "$": true,
    "jQuery": true
  },
}
// main.js
global.jQuery = require('jquery');
var $ = global.jQuery;
window.$ = $;

// component.vue
import $ from 'jquery'
import datepickerFactory from 'jquery-datepicker'
import datepickerJAFactory from 'jquery-datepicker/i18n/jquery.ui.datepicker-ja'
import 'jquery-ui/themes/base/all.css'

export default {
  // use the mounted lifecycle hook as this.$el is undefined until the component is mounted
  mounted: function() {
    var self = this
    $(this.$el).datepicker({
      dateFormat: this.dateFormat,
      onSelect: function(date) {
        self.$emit('update-date', date) // Passing data from jQuery to Vue
      }
    })
  },
  // use beforeDestroy to ensure our input is still in the DOM and thus can be selected (it's undefined in the destroy hook).
  beforeDestroy: function() {
    $(this.$el).datepicker('hide').datepicker('destroy');
  }
}
```

### [vue-moment](https://github.com/brockpetrie/vue-moment?ref=madewithvuejs.com)
Handy Moment.js filters for your Vue.js project.  
Moment.js's format: https://momentjs.com/docs/#/displaying/format/

$ npm install vue-moment

```javascript
<template>
  <span>{{ someDate | moment("dddd, MMMM Do YYYY, h:mm:ss a") }}</span>
  <!-- e.g. "Sunday, February 14th 2010, 3:25:50 pm" -->
</template>
<script>
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
Vue.use(VueMoment, {
    moment,
})
</script>
```

### [detect-browser](https://github.com/DamonOehlman/detect-browser)
Usage: detect a browser vendor and version (in a semver compatible format) using a navigator useragent in a browser or process.version in node

```javascript
import { detect } from 'detect-browser';
```
### [Cropper.js](https://github.com/fengyuanchen/cropperjs )
Usage: JavaScript image cropper

```javascript
import Cropper from 'cropperjs'
import 'cropperjs/src/css/cropper.scss'
```

### Global Event Bus
https://alligator.io/vuejs/global-event-bus/

### Axios 
provides is the ability to create a base instance that allows you to share a common base URL and configuration across all calls to the instance

```javascript
import axios from 'axios';

export const HTTP = axios.create({
  baseURL: `http://jsonplaceholder.typicode.com/`,
  headers: {
    Authorization: 'Bearer {token}'
  }
})
```