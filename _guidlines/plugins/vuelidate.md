## Vuelidate

Installation
$ npm install vuelidate --save

There are two ways how to integrate Vuelidate into our Vue.js app. 
We can either use it globally as a Vue plugin Vue.use(Vuelidate) or as a mixin. 

```javascript
import Vuelidate from 'vuelidate';
import { validationMixin } from 'vuelidate';

Vue.use(Vuelidate);
```

### Built-In Validators
Validation information will be stored in this.$v[propertyName].

```javascript
$v[propertyName]: {
  $dirty: boolean, // Whether or not this field has been interacted with yet.
  $invalid: boolean, // If this field is current valid or invalid, regardless of whether or not it is "dirty"
  $error: boolean, // Shortcut for $invalid && $dirty
  $pending: boolean, // Whether or not there is a pending validation (for async validations)
  $each: object // Holds all the validations for looped models.
  [YourValidationName]: boolean // The state of the validations defined on this property validations. (ie. if you have the 'required' validator, there would be a boolean 'required' property here, and so forth.)
  [Nested]: object // You can nest values in your model here as well, and the validation properties will be added to each nested field.
}
```
- required() - This field cannot be empty.
- minLength(length: number) - Minimum length of the field. Works on strings and arrays.
- maxLength(length: number) - Maximum length of the field. Works on strings and arrays.
- between(min, max) - Requires a number to be between the minimum and maximum values.
- alpha() - Only allows alphabetic characters.
- alphaNum() - Only allows alphanumeric characters.
- email() - Allow only valid emails.
- sameAs(fieldName: string | getFieldName: function -> string) - Allows you to require the input to be the same as another field, specified by fieldName or a function.
- or(validators…) - Valid when at least one of the specified validators is valid.
- and(validators…) - Valid when all of the specified validators are valid.

### Custom Validatiors
A custom validator in Vuelidate is simply a function that returns a boolean or a promise that resolves to a boolean.

```javascript
// Only allows inputField to contain 'rupert.'
export const MatchValidator = (stringToMatch) => {
  return (value, component) => value === stringToMatch;
}
```

```vue

```