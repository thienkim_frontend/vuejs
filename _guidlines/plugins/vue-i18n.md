### How to use vue-i18n plugin
https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-vue-app-with-vue-i18n
https://kazupon.github.io/vue-i18n/guide/formatting.html#custom-formatting

```javascript
/*
 * vue-i18n automatically replaces {count} and {n} in the translation with the number passed to the $tc() function
 * re-use translations, such as @:common.appname
*/
const en = 
en: {
  "hello": "Hallo {name}!",
  "text": "flower | flowers", // Add a | to separate singular and plural
  "wolves": "no wolf | one wolf | a pack of {n} wolves", // Zero, one or many
  "wolves2": "no wolf | one wolf | a pack of {numberOfWolves} wolves",
  "main-screen": {
    "title": "Main screen",
    "description": "This is the main screen..."
  },
  "common": {
    "appname": "Vue-Demo",
    "yes": "Yes",
    "no": "No"
  },
  "exit-dialog": {
    "title": "Exit screen",
    "text": "Do you really want to exit @:common.appname ?",
    "button-1": "@:common.yes",
    "button-2": "@:common.no"
  },
}

<template>
  <p>{{ $t('hello', { name: 'Vue.js'}) }}</p>
  <p>{{ $tc('text', 2) }}</p>
  <p>{{ $tc('wolves', 5) }}</p>
  <p>{{ $tc('wolves', 5, {numberOfWolves: 5}) }}</p>
  <p>{{ $t('about-screen.title') }}</p>
</template>
```

