import Vue from 'vue'
import Vuex from 'vuex'
import prerequisite from './modules/prerequisite'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        prerequisite: prerequisite,
    }
})
