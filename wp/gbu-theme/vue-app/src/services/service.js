import httpClient from 'axios'
import {APP_SERVICE_URL} from '../config'

class ServiceError {
    constructor(err, extra = null) {
        this.err = err
        this.extra = extra
    }

    getMessages(defaults = null) {
        if (this.err.e) {
            if (this.err.e.response) {
                if (!!this.err.e.response.statusText) return [this.err.e.response.statusText]
                switch (this.err.e.response.status) {
                    case 401:
                        return [defaults['401'] ? defaults['401'] : 'Unauthenticated!']
                    case 403:
                        return [defaults['403'] ? defaults['403'] : 'Forbidden error!']
                    case 404:
                        return [defaults['403'] ? defaults['404'] : 'Not found!']
                    case 500:
                        return [defaults['500'] ? defaults['500'] : 'Application error!']
                }
            }
            if (!!this.err.e.message) return [this.err.e.message]
            return [defaults['0'] ? defaults['0'] : 'Something went wrong!']
        }
        return this.err.m
    }
}

export default class Service {
    constructor(module = '') {
        this.module = module;
    }

    _url(relativeUrl = '') {
        if (this.module) {
            relativeUrl = this.module + '/' + relativeUrl
        }
        return APP_SERVICE_URL + '/' + relativeUrl
    }

    _done(response, doneCallback = null) {
        if (doneCallback) doneCallback(response.data)
    }

    _error(error, errorCallback = null) {
        if (errorCallback) {
            if (error.response && error.response.data) {
                errorCallback(new ServiceError({d: error.response.data}))
            } else {
                errorCallback(new ServiceError({e: error}))
            }
        }
    }

    _always(alwaysCallback = null) {
        if (alwaysCallback) alwaysCallback()
    }

    get(url, params = {}, doneCallback = null, errorCallback = null, alwaysCallback = null, cancelToken = null) {
        return httpClient.get(this._url(url), {
            params: params,
            cancelToken: cancelToken
        })
            .then((response) => {
                this._done(response, doneCallback)
            })
            .catch((error) => {
                this._error(error, errorCallback)
            })
            .then(() => {
                this._always(alwaysCallback)
            })
    }

    post(url, params = {}, doneCallback = null, errorCallback = null, alwaysCallback = null, cancelToken = null) {
        return httpClient.post(this._url(url), params, {
            cancelToken: cancelToken
        })
            .then((response) => {
                this._done(response, doneCallback)
            })
            .catch((error) => {
                this._error(error, errorCallback)
            })
            .then(() => {
                this._always(alwaysCallback)
            })
    }

    put(url, params = {}, doneCallback = null, errorCallback = null, alwaysCallback = null, cancelToken = null) {
        return httpClient.put(this._url(url), params, {
            cancelToken: cancelToken
        })
            .then((response) => {
                this._done(response, doneCallback)
            })
            .catch((error) => {
                this._error(error, errorCallback)
            })
            .then(() => {
                this._always(alwaysCallback)
            })
    }

    delete(url, doneCallback = null, errorCallback = null, alwaysCallback = null, cancelToken = null) {
        return httpClient.delete(this._url(url), {
            cancelToken: cancelToken
        })
            .then((response) => {
                this._done(response, doneCallback)
            })
            .catch((error) => {
                this._error(error, errorCallback)
            })
            .then(() => {
                this._always(alwaysCallback)
            })
    }

    deleteWithParams(url, params, doneCallback = null, errorCallback = null, alwaysCallback = null, cancelToken = null) {
        return httpClient.delete(this._url(url), {
            params: params,
            cancelToken: cancelToken,
        })
            .then((response) => {
                this._done(response, doneCallback)
            })
            .catch((error) => {
                this._error(error, errorCallback)
            })
            .then(() => {
                this._always(alwaysCallback)
            })
    }
}
