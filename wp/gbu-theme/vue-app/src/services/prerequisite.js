import Service from './service'

export default class PrerequisiteService extends Service {
    constructor() {
        super('prerequisite')
    }

    require(params = [], doneCallback = null, errorCallback = null, alwaysCallback = null) {
        let builtParams = {};
        params.forEach((param) => {
            builtParams[param] = 1;
        });
        this.get(
            '',
            builtParams,
            doneCallback,
            errorCallback,
            alwaysCallback,
        );
    }
}
