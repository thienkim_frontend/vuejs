import Service from './service'

export default class TestService extends Service {
    constructor() {
        super('test')
    }

    foo(params = {}, doneCallback = null, errorCallback = null, alwaysCallback = null) {
        this.get(
            'foo',
            params,
            doneCallback,
            errorCallback,
            alwaysCallback,
        );
    }
}
