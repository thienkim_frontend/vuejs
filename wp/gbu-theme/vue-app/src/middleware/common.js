import {Middleware} from '../plugins/middleware'
import intervalCaller from '../utils/timeout_caller'
import timeoutCaller from '../utils/timeout_caller'
import ui from '../utils/ui'
import session from '../utils/session'

class CommonMiddleware extends Middleware {
    handle($middlewareManager) {
        if ($middlewareManager.before) {
            timeoutCaller.clear()
            intervalCaller.clear()
            ui.startPageLoading()
        } else if ($middlewareManager.after) {
            ui.stopPageLoading()
            session.keep()
        }

        super.handle($middlewareManager)
    }
}

export default new CommonMiddleware()
