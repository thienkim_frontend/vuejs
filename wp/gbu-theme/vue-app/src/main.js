import Vue from 'vue'
import router from './router'
import store from './store'
import App from './views/App'
import Middleware from './plugins/middleware'
import middleware from './middleware'

Vue.config.productionTip = false

Vue.use(Middleware, {router, middleware})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
