class MiddlewareManager {
    constructor(groups) {
        this.groups = groups
        this.index = -1
    }

    run() {
    }

    handle() {
    }
}

export class BeforeMiddlewareManager extends MiddlewareManager {
    constructor(groups) {
        super(groups)
        this.before = true
    }

    run(to, from, next) {
        this.to = to
        this.from = from
        this.next = next
        this.index = -1

        this.handle()
    }

    handle() {
        if (this.groups.length === 0) {
            this.next();
            return;
        }
        ++this.index;
        if (this.index == this.groups.length) {
            this.next();
            return;
        }
        this.groups[this.index].handle(this);
    }
}

export class AfterMiddlewareManager extends MiddlewareManager {
    constructor(groups) {
        super(groups)
        this.after = true
    }

    run(to, from) {
        this.to = to
        this.from = from
        this.index = -1

        this.handle()
    }

    handle() {
        if (this.groups.length === 0) {
            return;
        }
        ++this.index;
        if (this.index == this.groups.length) {
            return;
        }
        this.groups[this.index].handle(this);
    }
}

export class Middleware {
    handle($middlewareManager) {
        $middlewareManager.handle()
    }

    redirect($middlewareManager, path) {
        $middlewareManager.next({
            path: path,
        })
    }
}

