import {BeforeMiddlewareManager, AfterMiddlewareManager} from './classes'

const install = (Vue, {router, middleware}) => {
    const beforeMiddlewareManager = new BeforeMiddlewareManager(middleware.before)
    const afterMiddlewareManager = new AfterMiddlewareManager(middleware.after)

    router.beforeEach((to, from, next) => {
        beforeMiddlewareManager.run(to, from, next)
    })

    router.afterEach((to, from) => {
        afterMiddlewareManager.run(to, from)
    })
}

export default install
