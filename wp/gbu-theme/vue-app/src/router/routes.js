import Base from '../views/master/Base'

export default [
  {
    path: '/',
    component: Base,
    children: [
      {
        path: '/',
        name: 'home',
        component: () => import('../views/pages/Home')
      },
      {
        path: '/about',
        name: 'about',
        component: () => import('../views/pages/About')
      },
      {
        path: '*',
        component: () => import('../views/error/NotFound')
      }
    ]
  }
]
