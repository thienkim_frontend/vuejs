export default {
  calls: [],

  register(handler, timeout = 1000) {
    let s = setInterval(handler, timeout)
    this.calls.push(s)
    return s
  },

  clear() {
    while (this.calls.length > 0) {
      clearInterval(this.calls.shift())
    }
  }
}
