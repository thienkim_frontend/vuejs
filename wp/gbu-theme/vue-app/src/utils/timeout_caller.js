export default {
  calls: [],

  register (handler, timeout = 200) {
    let s = setTimeout(handler, timeout)
    this.calls.push(s)
    return s
  },

  clear () {
    while (this.calls.length > 0) {
      clearTimeout(this.calls.shift())
    }
  }
}
