export default {
  _session: false, // Define false when user refreshes or comes at the first time

  keep (state = true) {
    this._session = state
  },

  isFresh () {
    return !this._session
  },

  isNotFresh () {
    return this._session
  }
}
