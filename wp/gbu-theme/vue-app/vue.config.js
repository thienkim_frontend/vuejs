module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/wp-content/themes/dsquare-theme/dist' : '/',
    outputDir: process.env.NODE_ENV === 'production' ? '../dist' : 'dist',
    css: {
        // drop the .module in the filenames
        modules: true,
        sourceMap: true,
        loaderOptions: {
            sass: {
                // @/ is an alias to src/
                data: `
                @import "@/assets/styles/_variables.scss";
                @import "@/assets/styles/_mixins.scss";
                `
            }
        }
    }
}
