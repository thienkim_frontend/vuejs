<?php
/**
 * DSquare Starter WP Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage DSquare_Starter_WP_Theme
 * @since 1.0.0
 */

add_theme_support('admin-bar', ['callback' => function () {
    ?>
    <style type="text/css" media="screen">
        html { margin-top: 32px !important; }
        * html body { margin-top: 32px !important; }
        .fixed-top { top: 32px !important; }
        @media screen and ( max-width: 782px ) {
            html { margin-top: 46px !important; }
            * html body { margin-top: 46px !important; }
            .fixed-top { top: 46px !important; }
        }
    </style>
    <?php
}]);

add_action('rest_api_init', function () {
    register_rest_route('dsquare-starter/v1', '/test/foo', array(
        'methods' => 'GET',
        'callback' => function () {
            return [
                'foo' => 'bar'
            ];
        },
    ));
});