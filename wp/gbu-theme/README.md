# DSquare Starter

A starter theme based on Vue.js for Wordpress  
WordPress: provide REST APIs to create, read, update and delete data in your WordPress database from your Vue.js interface

## Installation
1. Install MAMP on your Mac  
      https://wpshout.com/quick-guides/how-to-install-mamp-on-your-mac/

2. Install Wordpress  
      Download the lastes version   
      https://wordpress.org/download/  

      Locally Install WordPress  
      https://wpshout.com/quick-guides/install-wordpress-locally-mac/

      To login, add “/wp-admin” after the folder name  
      http://localhost:8888/wp-admin  
      http://localhost:88/phpMyAdmin/index.php?lang=en

Note: 
we won’t be deploying any of these downloaded files in WordPress. They are only required during the development phase.  

```
install the lastest npm version  
npm -v (check version)  
sudo npm install npm@latest -g  

cd {wordpress_directory}/wp-content/themes
git clone https://github.com/linhntaim/dsquare-starter-wp-theme.git
cd dsquare-theme/vue-app
npm install
cp src/config/index.js.example src/config/index.js
npm run build // output to dsquare-theme/dist
```

## Structure

```
style.css : theme detection (Wordpress required)
functions.php : REST API, functions, definitions, customizations (Wordpress required)
index.php : load file dist/index.html (Wordpress required)
vue-app : Vue-based SPA
      |---src
            |---config : configuration for app
                  |---index.js.example : example of configuration in local/dev environment
                  |---index.js.live : example of configuration in production environment
            |---middleware : handle access before and after each route request
                  |---index.js : list before and after middlewares used in app
                  |---common.js : example of a middleware 
            |---plugins : customized plugins for Vue
                  |---middleware : a self-dev middleware plugin for Vue
            |---router: route definition for app
                  |---index.js : router definition
                  |---routes.js : defails of routes in app
            |---services : api/service calling
                  |---service.js : base class for service definition
                  |---prerequisite.js : an example of service which request metadata from server (used by prerequisite store)
                  |---test.js : an example of service which request to foo if WP's REST API (see console log when running) 
            |---store : vuex
                  |---index.js : list store
                  |---modules : store modules
                        |---prerequisite.js : store metadata of app
            |---utils : utilities/helpers
            |---views : components
                  |---components : re-use/independent components
                  |---error : common error components (401, 404, 500, ...)
                  |---master : base templates
                  |---pages : route-based template
                  |---App.vue : app component
```

## Plugins Version
Wordpress 5.2.1   
Vue cli 3 https://cli.vuejs.org/guide/  
Axios https://github.com/axios/axios  
Vue 2.6.10 https://vuejs.org/v2/guide/  
Vue-router 3.0.3 https://router.vuejs.org/  
Vuex 3.0.1  https://vuex.vuejs.org/  
Pug 2.0.3 https://pugjs.org/api/getting-started.html  
Sass  https://sass-lang.com/   






